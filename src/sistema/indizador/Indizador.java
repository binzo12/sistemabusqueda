/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.indizador;

import sistema.archivos.Archivo;

/**
 * Esta clase contiene los atributos y metodos del Indizador.
 *
 * @author sam33
 * @version 1.0
 */
public class Indizador implements IAccesoIndizador{
    private String directorio;
    private String nombreArchivo;
    private String archivoPalabrasVacias;
    private String[] palabrasVacias;

    /**
     * Constructor de la clase indizador.
     * @param directorio nombre del directorio.
     * @param nombreArchivo nombre del archivo al analizar.
     * @param archivoPalabrasVacias nombre del archivo de PV.
     * @param palabrasVacias arreglo que contiene las palabras vacias.
     */
    public Indizador(String directorio, String nombreArchivo, String archivoPalabrasVacias, String[] palabrasVacias) {
        this.directorio = directorio;
        this.nombreArchivo = nombreArchivo;
        this.archivoPalabrasVacias = archivoPalabrasVacias;
        this.palabrasVacias = palabrasVacias;
    }

    /**
     * Métodos get y set para todos los atributos
     * @param nombreArchivo
     */
    
    public void setDatos(String nombreArchivo) {
        
    }
    
    public void setDirectorio() {
        
    }
    
    public void setPalabrasVacias(String archivoPalabrasVacias) {
        
    }
    
    public String[] getPalabrasVacias() {
        return null;
        
    }
    
    public String getDirectorio() {
        return null;
        
    }
    
    public String getDatos() {
        return null;
        
    }
    
    public String[] generarIndice(String palabra, String nombreArchivo) {
        return null;
        
    }
    
    public int contabilizarPalabras(String palabra, String datos[]){
        return 0;
    }

    @Override
    public void nuevoIndizador() {
        
    }
    
    public void getPrototype(){
        
    }
    
}
