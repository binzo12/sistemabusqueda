/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.principal;

import sistema.buscador.Buscador;
import sistema.indizador.Indizador;

/**
 *
 * @author sam33
 */
public class SistemaFacade {
    public static Indizador indice;
    public static Buscador buscar;
    
    /**
     * Metodos
     */
    public void iniciarIndizador(){
        indice.nuevoIndizador();
    }
    
    public void iniciarBuscador(){
    }
    
}
