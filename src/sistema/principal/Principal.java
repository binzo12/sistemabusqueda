/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.principal;

/**
 *
 * @author sam33
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("Bienvenido al sistema de busqueda de cadenas");
        
        if(args.length == 1 ){
            SistemaFacade indizador = new SistemaFacade();
            indizador.iniciarIndizador();
        }else{
            if(args.length > 1){
                SistemaFacade buscador = new SistemaFacade();
                buscador.iniciarBuscador();
            }
        }
    }
    
}
