/**
 * Archivo.java
 * Clase creada el 20 de mayo del 2018, 2:40 AM.
 */
package sistema.archivos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sam33
 */
public class Archivo {
    /**
     * SE DEBE CAMBIAR EN LOS DIAGRAMAS.
     * Directorio donde se va a buscar el archivo.
     */
    private String directorio;
    /**
     * SE DEBE CAMBIAR EN LOS DIAGRAMAS.
     * Arreglo que contiene los datos leidos del archivo.
     */
    public ArrayList<String> datos;
    /**
     * Constructor predeterminado de la clase.
     */
    public Archivo() {
    }
    /**
     * @param directorio Nombre del archivo de texto.
     */
    public Archivo(String directorio){
        this.directorio = directorio;
        this.datos = new ArrayList<>();
    }

    //Metodos de la clase.
    /**
     * CAMBIAR EN EL DIAGRAMA DE CLASES
     * Metodos para escribir en un archivo de texto.
     * @param directorio direccion donde se va a guardar el nuevo archivo.
     * @param datos Datos a introducir en el archivo de texto.
     */
    public void escribirArchivo(String directorio,ArrayList<String> datos) {
        try {
            FileWriter fr = new FileWriter(directorio);
            PrintWriter pw = new PrintWriter(fr);
            
            for (int i = 0; i < datos.size(); i++) {
                pw.println(datos.get(i));
            }
            
            pw.close();
        } catch (IOException e) {
            System.out.println("Error al imprimir...");
        }
    }
    /**
     * CAMBIAR EN EL DIAGRAMA DE CLASES.
     * Metodo para leer un archivo de texto.
     * @param nombre Nombre del archivo de texto a leer.
     * @return datos Retorna el arreglo con los datos obtenidos.
     */
    public ArrayList<String> leerArchivo(String nombre) {
        try {
            
            FileReader fr = new FileReader(nombre);
            BufferedReader br = new BufferedReader(fr);
            String linea = "";
            
            while ((linea = br.readLine()) != null) {
                datos.add(linea);   
            }
            fr.close();
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado: " + this.directorio);
        } catch (IOException ex) {
            Logger.getLogger(Archivo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos;
    }
    /**
     * CAMBIAR EN EL DIAGRAMA DE CLASES.
     * Metodo para obtener el arreglo.
     * @return datos Retorna el arreglo que contiene los datos leido.
     */
    public ArrayList<String> getDatos() {
        return datos;
    }
    
}
