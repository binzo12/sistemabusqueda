
package sistema.archivos;

import java.util.ArrayList;

/**
 *
 * @author sam33
 */
public class ArchivoConfiguracion extends Archivo{

    /**
     * CAMBIAR EN EL DIAGRAMA DE CLASES
     * Direccion del directorio donde se va a realizar
     * la busqueda.
     */
    private String directorioAB;
    /**
     * Nombre del archivo que contendra
     * las palabras vacias.
     */
    private String nombrePalabrasVacias;
    /**
     * Atributo que ayudara a obtener
     * una instancia del objeto si este
     * no existe.
     */
    private static ArchivoConfiguracion archivoConfiguracion;

    /**
     * CAMBIAR EN EL DIAGRAMA DE CLASES.
     */
    private ArchivoConfiguracion() {
        super();
    }


    /**
     *CAMBIAR EN EL DIAGRAMA DE CLASES.
     * @return archivoConfiguracion.
     */
    public static ArchivoConfiguracion getInstance() {

        if (archivoConfiguracion == null) {
            archivoConfiguracion = new ArchivoConfiguracion();
        } else {

        }
        return archivoConfiguracion;
    }
    /**
     * Metodo que evita la clonacion de objetos.
     * @return
     */
    @Override
    public ArchivoConfiguracion clone(){
        try {
            throw new CloneNotSupportedException();

        } catch (CloneNotSupportedException ex) {
            System.out.println("No se puede clonar el objeto");
        }
        return null;
    }
    /**
     * Metodos que establece el directorio.
     * @param datos Datos de donde obtendra el directorio
     */
    public void setDirectorio(ArrayList<String> datos) {

        for (int i = 0; i < datos.size() ; i++) {
            String[] palabra = datos.get(i).split(" ");

            if (palabra[0].equals("Directorio")) {
                this.directorioAB = datos.get(i);
            }
        }
    }

    /**
     * Metodo que establece las palabras vacias.
     * @param datos
     */
    public void setArchivoPalabrasVacias(ArrayList<String> datos) {

        for (int i = 0; i < datos.size(); i++) {
            String[] palabra = datos.get(i).split(" ");

            if (palabra[0].equals("Palabras") && palabra[1].equals("Vacias")) {
                this.nombrePalabrasVacias = datos.get(i);
            }
        }
    }

    public String getDirectorio() {
        return directorioAB;
    }

    public String getArchivoPalabrasVacias() {
        return nombrePalabrasVacias;
    }

}
